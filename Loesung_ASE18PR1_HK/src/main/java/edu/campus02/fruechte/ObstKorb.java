package edu.campus02.fruechte;

import java.util.ArrayList;

public class ObstKorb {
	private ArrayList<Frucht> fruechte = new ArrayList<Frucht>();

	public void fruchtAblegen(Frucht f) {
		fruechte.add(f);
	}

	public int zaehleFruechte(String sorte) {
		int anzahl = 0;
		for (Frucht frucht : fruechte) {
			if (frucht.getSorte().equals(sorte))
				anzahl++;
		}

		return anzahl;
	}

	public Frucht schwersteFrucht() {
		Frucht schwersteFrucht = fruechte.get(0);
		for (Frucht frucht : fruechte) {
			if (schwersteFrucht.getGewicht() < frucht.getGewicht())
				schwersteFrucht = frucht;
		}

		return schwersteFrucht;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht) {
		ArrayList<Frucht> result = new ArrayList<Frucht>();

		for (Frucht frucht : fruechte) {
			if (frucht.getGewicht() > gewicht)
				result.add(frucht);
		}
		return result;
	}

	public int zaehleSorte() {
		ArrayList<String> verschiedeneSorten = new ArrayList<String>();

		for (Frucht frucht : fruechte) {
			if (!verschiedeneSorten.contains(frucht.getSorte()))
				verschiedeneSorten.add(frucht.getSorte());
		}

		return verschiedeneSorten.size();
	}

	public String toString() {
		return fruechte.toString();

	}
}
