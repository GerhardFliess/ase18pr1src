package edu.campus02.dosen;

import java.util.ArrayList;

public class Keksdose {
	private double breite;
	private double laenge;
	private double hoehe;
	private String farbe;

	private Keksdose innereDose;
	private double volumen;
	private double freiesVolumen;

	private ArrayList<Keks> kekse = new ArrayList<Keks>();

	public Keksdose(String farbe, double x, double y, double hoehe) {
		this.farbe = farbe;
		this.hoehe = hoehe;
		if (x < y) {
			this.breite = x;
			this.laenge = y;
		} else {
			this.laenge = x;
			this.breite = y;
		}

		volumen = x * y * hoehe;
		freiesVolumen = volumen;
	}

	public double getBreite() {
		return breite;
	}

	public double getLaenge() {
		return laenge;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double volumen() {
		return volumen;
	}

	public double freiesVolumen() {
		return freiesVolumen;
	}

	public boolean keksAblegen(Keks keks) {
		if (freiesVolumen() >= keks.getVolumen()) {
			kekse.add(keks);
			freiesVolumen = freiesVolumen - keks.getVolumen();
			return true;
		} else {
			return false;
		}
	}

	public boolean leer() {
		return kekse.isEmpty();
	}

	public String toString() {
		if (innereDose == null) {
			return String.format("(%s %.1f x %.1f x %.1f)", farbe, breite, laenge, hoehe);
		} else {
			return String.format("(%s %.1f x %.1f x %.1f %s)", farbe, breite, laenge, hoehe, innereDose);

		}
	}

	public boolean stapeln(Keksdose d) {

		if (!leer())
			return false;

		if (d.getBreite() >= this.breite)
			return false;

		if (d.getLaenge() >= this.laenge)
			return false;

		if (d.getHoehe() >= this.hoehe)
			return false;

		if (innereDose == null) {
			innereDose = d;
			return true;
		} else {
			return innereDose.stapeln(d);
		}
	}

	public double gesamtVolumen() {
		double meinVolumen = volumen();
		if (innereDose == null) {
			return meinVolumen;
		} else {
			return meinVolumen + innereDose.gesamtVolumen();
		}
	}
}
