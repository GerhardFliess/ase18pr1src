package edu.campus02.dosen.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.campus02.dosen.Keks;
import edu.campus02.dosen.Keksdose;

public class KeksdosenTest {
	@Test
	public void construktorBreiteHoehe() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		assertEquals(20, d1.getBreite(), 0.01);
		assertEquals(30, d1.getLaenge(), 0.01);
		assertEquals(10, d1.getHoehe(), 0.01);
	}

	@Test
	public void construktorBreiteHoeheTauschen() throws Exception {
		Keksdose d2 = new Keksdose("rot", 30, 20, 10);
		assertEquals(20, d2.getBreite(), 0.01);
		assertEquals(30, d2.getLaenge(), 0.01);
		assertEquals(10, d2.getHoehe(), 0.01);
	}

	@Test
	public void volumen01() throws Exception {
		Keksdose d2 = new Keksdose("rot", 30, 20, 10);

		assertEquals(6000, d2.volumen(), 0.01);
	}

	@Test
	public void toStringTest01() throws Exception {
		Keksdose d2 = new Keksdose("rot", 20, 30, 10);
		assertEquals("(rot 20,0 x 30,0 x 10,0)", d2.toString());
	}

	@Test
	public void kekse01() throws Exception {
		Keksdose d1 = new Keksdose("gruen", 10, 10, 10);

		assertTrue(d1.leer());

		d1.keksAblegen(new Keks("Vanille Kipferl", 10, 20));

		assertFalse(d1.leer());
	}

	@Test
	public void kekse02() throws Exception {
		Keksdose d1 = new Keksdose("gruen", 10, 10, 10);

		assertEquals(1000, d1.freiesVolumen(), 0.1);

		d1.keksAblegen(new Keks("Vanille Kipferl", 10, 20));

		assertEquals(980, d1.freiesVolumen(), 0.1);
	}

	@Test
	public void kekse03() throws Exception {
		Keksdose d1 = new Keksdose("gruen", 10, 10, 10);

		assertEquals(1000, d1.freiesVolumen(), 0.1);

		for (int zaehler = 0; zaehler < 50; zaehler++) {
			assertTrue(d1.keksAblegen(new Keks("Vanille Kipferl", 10, 20)));
		}

		assertEquals(0, d1.freiesVolumen(), 0.1);

		assertFalse(d1.keksAblegen(new Keks("Vanille Kipferl", 10, 20)));

	}

	@Test
	public void stapelbar01() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("rot", 10, 20, 5);

		assertTrue(d1.stapeln(d2));
	}

	@Test
	public void stapelbar02() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("rot", 10, 20, 5);

		assertFalse(d2.stapeln(d1));
	}

	@Test
	public void stapelbar03() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("rot", 20, 30, 10);

		assertFalse(d2.stapeln(d1));
	}

	@Test
	public void stapelbar04() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("rot", 20, 30, 10);

		d1.keksAblegen(new Keks("Vanille Kipferl", 10, 20));
		assertFalse(d2.stapeln(d1));
	}

	@Test
	public void toStringTest02() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("blau", 10, 20, 5);
		assertTrue(d1.stapeln(d2));

		assertEquals("(rot 20,0 x 30,0 x 10,0 (blau 10,0 x 20,0 x 5,0))", d1.toString());
	}

	@Test
	public void volumen02() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 10);
		Keksdose d2 = new Keksdose("blau", 10, 20, 5);

		d1.stapeln(d2);

		assertEquals(7000, d1.gesamtVolumen(), 0.01);
	}

	@Test
	public void gesamtVolumen01() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 20);
		Keksdose d2 = new Keksdose("blau", 10, 20, 10);
		Keksdose d3 = new Keksdose("gruen", 5, 10, 5);

		d1.stapeln(d2);
		assertTrue(d1.stapeln(d3));

		assertEquals(14250, d1.gesamtVolumen(), 0.01);
	}

	@Test
	public void gesamtVolumen02() throws Exception {
		Keksdose d1 = new Keksdose("rot", 20, 30, 20);
		Keksdose d2 = new Keksdose("blau", 10, 20, 10);
		Keksdose d3 = new Keksdose("gruen", 10, 20, 10);

		d1.stapeln(d2);
		assertFalse(d1.stapeln(d3));

		assertEquals(14000, d1.gesamtVolumen(), 0.01);
	}

}
