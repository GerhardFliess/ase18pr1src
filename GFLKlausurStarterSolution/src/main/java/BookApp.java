import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp {
	public static void main(String[] args) {
		Bibliothek bibliothek = new Bibliothek();

		bibliothek.addBook(new Book("Hofstadter", "Metamagikum", 600));
		bibliothek.addBook(new Book("Lafer", "Meine heimatküche", 200));
		bibliothek.addBook(new Book("Knuth", "The art of computer programming", 200));

		System.out.println(bibliothek.countPages());
	}
}
