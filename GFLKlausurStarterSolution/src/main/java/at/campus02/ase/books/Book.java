package at.campus02.ase.books;

public class Book {

	private String autor;
	private String title;
	private int pages;

	public Book(String autor, String titel, int seiten) {
		this.autor = autor;
		this.title = titel;
		this.pages = seiten;
	}

	public String getAutor() {
		return autor;
	}

	public String getTitel() {
		return title;
	}

	public int getSeiten() {
		return pages;
	}

	public boolean match(String search) {
		if (search == null)
			return false;

		if (autor.contains(search))
			return true;

		if (title.contains(search))
			return true;

		return false;
	}

	public String toString() {
		return String.format("[%s,%s,%d]", getAutor(), getTitel(), getSeiten());
	}
}
