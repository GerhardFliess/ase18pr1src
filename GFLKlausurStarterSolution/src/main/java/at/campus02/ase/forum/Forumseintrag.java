package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {

	private String titel;
	private String text;
	private ArrayList<Forumseintrag> replies = new ArrayList<Forumseintrag>();

	public Forumseintrag(String titel, String text) {
		this.titel = titel;
		this.text = text;
	}

	public Forumseintrag antworten(String titel, String text) {
		Forumseintrag newReply = new Forumseintrag(titel, text);
		replies.add(newReply);

		return newReply;
	}

	public ArrayList<Forumseintrag> getAntworten() {
		return replies;
	}

	public int anzahlDerEintraege() {
		int sumReplys = 0;

		for (Forumseintrag forumseintrag : replies) {
			sumReplys = sumReplys + 1 + forumseintrag.anzahlDerEintraege();
		}
		return sumReplys;
	}

	public String toString() {
		String antworten = "";
		for (Forumseintrag forumseintrag : replies) {
			antworten = antworten + forumseintrag.toString();
		}
		if (!antworten.isEmpty())
			antworten = "[" + antworten + "]";
		
		return String.format("(%s,%s)%s", titel, text, antworten);
	}
}
