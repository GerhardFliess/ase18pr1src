package edu.campus02.automat;

public class CreationUtil {
	public static State create01() {

		State start = new State("start");

		State s1 = new State("s1");
		State s2 = new State("s2");
		State end = new State("end", true);

		start.addTransition(new Transition("a", s1));
		start.addTransition(new Transition("b", s2));

		s1.addTransition(new Transition("a", s1));
		s1.addTransition(new Transition("c", end));

		s2.addTransition(new Transition("b", s2));
		s2.addTransition(new Transition("c", end));

		return start;
	}
	
	
	public static State create02() {

		State start = new State("start");

		State s1 = new State("s1");
		State s2 = new State("s2");
		State end = new State("end", true);

		start.addTransition(new Transition("ab", s1));
		start.addTransition(new Transition("c", s2));

		s1.addTransition(new Transition("ab", s1));
		s1.addTransition(new Transition("c", end));

		s2.addTransition(new Transition("c", s2));
		s2.addTransition(new Transition("d", end));

		return start;
	}
	
}
