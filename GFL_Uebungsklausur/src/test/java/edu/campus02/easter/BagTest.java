package edu.campus02.easter;

import static org.junit.Assert.*;

import org.junit.Test;

public class BagTest {

	Egg[] eggs = new Egg[] { new Egg(75, "red"), new Egg(70, "red"), new Egg(78, "red"), new Egg(80, "blue"),
			new Egg(75, "blue"), new Egg(60), new Egg(61), new Egg(62) };

	@Test
	public void testPut() throws Exception {

		Bag bag = new Bag();
		putEggs(bag);

		assertEquals(8, bag.countEggs());
	}

	@Test
	public void testSum() throws Exception {

		Bag bag = new Bag();
		putEggs(bag);

		assertEquals(561, bag.sumWeight(), 0.01);
	}

	@Test
	public void testSumWhite() throws Exception {

		Bag bag = new Bag();
		putEggs(bag);

		assertEquals(183, bag.sumWeight("weiss"), 0.01);
	}
	@Test
	public void testDifferentColors() throws Exception {

		Bag bag = new Bag();
		putEggs(bag);

		assertEquals(3, bag.countDifferentColors());
	}
	
	private void putEggs(Bag bag) {
		for (Egg egg : eggs) {
			bag.putEgg(egg);
		}

	}

}
