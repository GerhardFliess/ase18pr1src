package org.campus.uno;

import java.util.ArrayList;

public class Spieler
{
	private String name;

	private ArrayList<Karte> handkarten = new ArrayList<Karte>();

	public Spieler(String name)
	{
		this.name = name;
	}

	public void aufnehmen(Karte karte)
	{
		handkarten.add(karte);
	}

	public String toString()
	{
		return name+ " "+handkarten;
	}

	public Karte passendeKarte(Karte vergleichsKarte)
	{

		for (Karte karte : handkarten)
		{
			if (karte.match(vergleichsKarte))
			{
				handkarten.remove(karte);
				if (handkarten.size() == 1)
				{
					System.out.println(" UNO");
				}
				return karte;
			}
		}

		return null;
	}

	public int anzahlDerHandkarten()
	{
		return handkarten.size();
	}

}
