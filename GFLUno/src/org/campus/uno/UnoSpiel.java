package org.campus.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();

	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	public UnoSpiel()
	{
		for (int index = 0; index < 2; index++)
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++)
			{
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert));
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
			}
		}

		Collections.shuffle(kartenStapel); // Karten mischen

	}

	public Karte abheben() // liefert eine Karte zur�ck
	{
		// sind noch genug karten am stapel?
		
		if(kartenStapel.size()==0)
		{   
			//oberste karte merken
			Karte obersteKarte = ablageStapel.remove(0);
			
			Collections.shuffle(ablageStapel); // restlichen stapel mischen
			
			//Aus ablagestepel neuen kartenstapel machen
			kartenStapel.addAll(ablageStapel); // alle Karten einf�gen
			
			ablageStapel.clear(); // kartenstapel leeren und
			ablageStapel.add(obersteKarte); // oberste karte wieder drauf legen
			
		}
		
		return kartenStapel.remove(0);
	}

	public void karteAblegen(Karte ablegeKarte)
	{
		ablageStapel.add(0, ablegeKarte);// Karte an der Stelle 0 einf�gen
	}

	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}

	public boolean spielzug()
	{
		// wer ist dran?
		Spieler aktuellerSpieler = mitspieler.remove(0);
		System.out.println("am Zug ist:" + aktuellerSpieler);

		// spieler vergleicht die oberste karte am ablagestapel mit den handkarten
		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0));

		if (gefundeneKarte != null)
		{
			System.out.println(" karte ablegen:" + gefundeneKarte);
			karteAblegen(gefundeneKarte);
		} else
		{
			System.out.println(" karte ziehen");
			Karte abgehobeneKarte = abheben();

			if (abgehobeneKarte.match(ablageStapel.get(0)))
			{
				System.out.println(" ablegen:" + abgehobeneKarte);
				karteAblegen(abgehobeneKarte);
			} else
			{
				System.out.println(" karte aufnehmen");
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
		}

		if (aktuellerSpieler.anzahlDerHandkarten() == 0)
		{
			System.out.println("gewonnen hat:" + aktuellerSpieler);
			return false;
		}

		// spielzug ende spiler objekt als letztes in die liste einf�gen
		mitspieler.add(aktuellerSpieler);
		return true;

	}

	public void austeilen()
	{
		for (int zaehler = 0; zaehler < 7; zaehler++)
		{
			// Jeden Mitspieler einmal "besuchen"
			for (Spieler sp : mitspieler)
			{
				Karte karte = abheben();
				sp.aufnehmen(karte);
			}
		}

		Karte temp = abheben();
		ablageStapel.add(temp);
		System.out.println(temp);
	}

}
