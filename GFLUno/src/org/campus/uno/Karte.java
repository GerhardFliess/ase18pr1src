package org.campus.uno;

public class Karte
{
	private Farbe farbe;
	private int zahl;

	public Karte(Farbe f, int z)
	{
		farbe = f;
		zahl = z;
	}

	public boolean match(Karte passtDu)
	{
		if (farbe == passtDu.farbe || zahl == passtDu.zahl)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean compare(Karte zweiteKarte)
	{
		if (farbe == zweiteKarte.farbe) // wenn die Farben gleich sind
		{
			// zahlen vergleichen
			boolean result = zahl > zweiteKarte.zahl;
			return result;

			// retrun zahl > zweiteKarte.zahl; // kurze schreibweise
		} else
		{
			// ordinal gibt einen Zahlenwert mit der Stelle im enum zur�ck
			return farbe.ordinal() < zweiteKarte.farbe.ordinal();
		}	
	}
	
	public String toString()
	{
		return farbe + " " + zahl;
	}
}
