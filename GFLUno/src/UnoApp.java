import org.campus.uno.Farbe;
import org.campus.uno.Karte;
import org.campus.uno.Spieler;
import org.campus.uno.UnoSpiel;

public class UnoApp
{

	public static void main(String[] args)
	{
		UnoSpiel meinSpiel = new UnoSpiel();

		Spieler sp1 = new Spieler("Maverick");
		Spieler sp2 = new Spieler("Annabel");

		meinSpiel.mitSpielen(sp1);
		meinSpiel.mitSpielen(sp2);
		meinSpiel.mitSpielen(new Spieler("Zane Cooper"));

		meinSpiel.austeilen();

		int z�hler = 1; //Spielz�ge mitz�hlen
		while (meinSpiel.spielzug())
		{
			System.out.print(z�hler + " ");
			z�hler++;
		}

		// System.out.println(sp1.passendeKarte(new Karte(Farbe.blau, 1)));

		// Karte abgehobeneKarte = meinSpiel.abheben();
		// System.out.println(abgehobeneKarte);
		// System.out.println(meinSpiel.abheben());
	}

}
