import java.util.ArrayList;

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenArray
{

	public static void main(String[] args)
	{
		int[] zahle = new int[]
		{ 1, 2, 3, 4 };

		Karte[] meineKarten = new Karte[]
		{ new Karte(Farbe.rot, 1), new Karte(Farbe.blau, 1), new Karte(Farbe.gelb, 10) };

		System.out.println(meineKarten[1]);

		for (Karte karte : meineKarten)
		{
			System.out.println(karte);
		}

		// Type ArrayList die Karten verwalten kann
		ArrayList<Karte> meineListe = new ArrayList<Karte>();

		// eine Karte in die Liste einf�rgen
		meineListe.add(new Karte(Farbe.rot, 2));
		meineListe.add(new Karte(Farbe.gruen, 2));
		meineListe.add(new Karte(Farbe.blau, 3));

		// gibt mir die karte an der position 1
		System.out.println(meineListe.get(1));
		System.out.println();
		// eine liste mit einer Schleife ausgeben
		for (Karte karte : meineListe)
		{
			System.out.println(karte);
		}

		System.out.println(meineListe.size());

		System.out.println(meineListe.remove(0));

		System.out.println(meineListe.size());

		System.out.println(meineListe);

		ArrayList<Integer> zahlenListe; // statt int Integer dann kann man auch Zahlen in listen verwalten

	}

}
