
public class ArrayExample
{

	public static void main(String[] args)
	{
		int[] feld1 = new int[]
		{ 5, 2, 4, 1, 3, 8, 6, 2, 3, 4, 6 };

		int[] feld2 = feld1;
		
		int stelle = 4;
		System.out.println(feld1[stelle]); // index aus einer variable
		System.out.println(feld1[3]); // wert an der stelle 3;
		System.out.println(feld1.length); // l�nge vom feld

		printArray(feld1);
		System.out.println();

		ArrayMax.findMax(feld1); // verweis auf eine andere Klasse

		printArray(new int[]
		{ 1, 2, 3, 4, 5, 6, 7 });
		
		System.out.println();
		printArray(feld1); // ausgabe vor dem Tauschen
		swap(feld1, 2, 3); // tauschen
		printArray(feld1); // nach dem Tauschen
		swap(feld1, 6, 1); // tauschen
		printArray(feld1); // nach dem Tauschen
	}

	public static void printArray(int[] feld)
	{
		for (int index = 0; index < feld.length; index++) // alle inhalte aus dem Feld ausgeben
		{
			System.out.print(feld[index] + " ");
		}
		System.out.println();
	}

	// Schreiben sie eine Methode die in einem Feld zwei stellen vertauscht
	// die Methode hat das Feld und die erste und zweite stelle als Parameter
	public static void swap(int[] feld, int a, int b)
	{
		int temp = feld[a]; // wert an 1. Stelle merken
		feld[a] = feld[b];  // wert von 2. Stelle auf 1. Stelle schreiben
		feld[b] = temp;     // gemerkten Wert an die 2. Stelle schreiben
	
	}

}
