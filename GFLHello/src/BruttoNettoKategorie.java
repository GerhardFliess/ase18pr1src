
public class BruttoNettoKategorie
{

	public static void main(String[] args)
	{
		double nettoPreis = 100;
		double brutto = nettoPreis;

		int steuerKategorie = 2; // 2 oder 3

		brutto = bruttoPreis(321, 3); // aufrufen der Methode
		System.out.println(brutto);

		brutto = bruttoPreis(200, 1);
		System.out.println(brutto);

		// da kommt jetzt das If...
		if (steuerKategorie == 1)
		{
			brutto = nettoPreis * 1.2; // 20% steuer
		} else if (steuerKategorie == 2)
		{
			brutto = nettoPreis * 1.12; // 12% steuer
		} else if (steuerKategorie == 3)
		{
			brutto = nettoPreis * 1.1; // 10% steuer
		}

		System.out.println(brutto);
	}

	// datentyp
	// r�ckgabewert namen void kein r�ckgabe wert
	public static double bruttoPreis(double netto, int kategorie)
	{
		double result;
		switch (kategorie)
		{
		case 1:
			result = netto * 1.2; // 20% steuer
			break;
		case 2:
			result = netto * 1.12; // 12% steuer
			break;
		case 3:
			result = netto * 1.1; // 10% steuer
			break;
		default:
			result = netto;
			break;
		}

		return result; // return liefert den wert zur�ck
	}

}
