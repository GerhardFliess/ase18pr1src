
public class Konto
{

	public static void main(String[] args)
	{
		printKonto(2000, 2.5, 10);
	}

	public static void printKonto(double einlage, double zinsen, int laufzeit)
	{
		double zinssatz = 1 + zinsen / 100;
		double kontostand = einlage;

		for (int jahre = 1; jahre <= laufzeit; jahre++)
		{
			kontostand = kontostand * zinssatz;
			System.out.println(jahre + " " + kontostand);
		}
	}
	
}
