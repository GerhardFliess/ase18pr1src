
public class ForEach
{

	public static void main(String[] args)
	{
		int[] zahlen = new int[]
		{ 1, 3, 5, 1, 7, 22, 33, 42 };

		// name der variable : feld das ist durchlaufen will
		for (int wert : zahlen) // die wird so oft durchlaufen wir das feld lang ist
		{
			System.out.println(wert); // bei jedem durchlauf hat wert den Zahlen wert
										// an der stelle im feld
		}
		System.out.println();

		int summe = 0;
		for (int zahlenwert : zahlen)
		{
			summe = summe + zahlenwert;
		}
		System.out.printf("Die summe ist %d \n" , summe);

		// for (int stelle = 0; stelle < zahlen.length; stelle++)
		// {
		// int wert = zahlen[stelle];
		// }

	}

}
