package edu.campus02.easter;

public class Egg {

	private String color = "weiss";
	private double weight = -1;

	public Egg(double weight) {
		this.weight = weight;
	}

	public Egg(double weight, String color) {
		this.weight = weight;
		this.color = color;
	}

	public double getWeight() {
		return weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String toString() {
		return String.format("(%.2f %s)", weight, color);
	}
}
