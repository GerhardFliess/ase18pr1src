package edu.campus02.easter;

import java.util.ArrayList;

public class Bag
{

	private ArrayList<Egg> eggs = new ArrayList<Egg>();

	public void putEgg(Egg egg)
	{
		eggs.add(egg);
	}

	public int countEggs()
	{
		return eggs.size();
	}

	public double sumWeight()
	{
		double result = 0;
		for (Egg egg : eggs)
		{
			result += egg.getWeight();
		}
		return result;
	}

	public double sumWeight(String color)
	{

		double result = 0;
		for (Egg egg : eggs)
		{
			if (color.equals(egg.getColor()))
			{
				result += egg.getWeight();
			}
		}
		return result;
	}

	public int countDifferentColors()
	{
		ArrayList<String> differentColors = new ArrayList<String>();
		for (Egg egg : eggs)
		{
			if (!differentColors.contains(egg.getColor()))
			{
				differentColors.add(egg.getColor());
			}
		}
		return differentColors.size();
	}

}
