package edu.campus02.automat;

public class ValidateUtil
{

	public static boolean verify(State start, String word)
	{
		char[] charArray = word.toCharArray();
		State currentState = start;
		for (char c : charArray)
		{
			Transition matchingTransition = currentState.findMatchingTransition(c);
			if (matchingTransition != null)
			{
				currentState = matchingTransition.getTarget();
			} else
			{
				return false;
			}
		}

		return currentState.isFinal();
	}

	public static boolean verifyRekursive(State state, String word)
	{
		if (word.isEmpty())
		{
			return state.isFinal();
		}

		char charAt = word.charAt(0);
		Transition matchingTransition = state.findMatchingTransition(charAt);

		if (matchingTransition == null)
			return false;

		return verifyRekursive(matchingTransition.getTarget(), word.substring(1));
	}

}
