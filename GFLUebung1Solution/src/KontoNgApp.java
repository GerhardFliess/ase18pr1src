
public class KontoNgApp {

	public static void main(String[] args) {

		System.out.printf("%.2f ", calcEarning(2000, 1.5, 10));
	}

	public static double calcEarning(double konto, double zinssatz, int jahre) {
		double result = konto;

		for (int count = 0; count < jahre; count++) {
			result = result + result * zinssatz / 100;
		}
		return result;
	}

}
