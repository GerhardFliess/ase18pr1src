
public class Mathe {

	public static void main(String[] args) {

		int zahl = 623;

		int ergebnis = berechneZiffernsumme(zahl);
		System.out.println(ergebnis);

		System.out.println(berechneZiffernsumme(1024));
		System.out.println(berechneZiffernsumme(4321));

		int werte[] = { 1, 4, 3, 5, 7, 1, 2, 3, 5, 7 };
		double mittelt = berechneMittelwert(werte);

		System.out.println(mittelt);
		System.out.printf("1 kommt %d mal vor \n", haeufigkeit(werte, 1));
		System.out.printf("9 kommt %d mal vor \n", haeufigkeit(werte, 9));

		System.out.printf("es gibt %d verschiedene Zahle", verschiedeneWerte(werte));

		String test = "Hansi75";
		
		int testint=1;
		
		
		test.toLowerCase();
		System.out.println(test.charAt(2));
	}

	public static int max(int[] werte) {
		int max = werte[0];
		for (int wert : werte) {
			if (max < wert)
				max = wert;
		}
		return max;

	}

	public static int min(int[] werte) {
		int min = werte[0];
		for (int wert : werte) {
			if (min > wert)
				min = wert;
		}
		return min;

	}

	public static int haeufigkeit(int[] werte, int zahl) {
		int counter = 0;

		for (int wert : werte) {
			if (wert == zahl) {
				counter++;
			}
		}

		return counter;
	}

	public static double berechneMittelwert(int[] werte) {
		double ergebnis = 0;

		for (int wert : werte) {
			ergebnis = ergebnis + wert;
		}
		ergebnis = ergebnis / werte.length;
		return ergebnis;
	}

	public static int berechneZiffernsumme(int zahl) {
		int ziffernsumme = 0;

		while (zahl > 0) {
			int temp = zahl % 10;
			ziffernsumme = ziffernsumme + temp;

			zahl = zahl / 10;
		}

		return ziffernsumme;
	}

	public static int verschiedeneWerte(int[] werte) {
		int count = 0;
		int min = min(werte);
		int max = max(werte);
		for (int index = min; index <= max; index++) {
			if (haeufigkeit(werte, index) != 0)
				count++;
		}

		return count;
	}

}
