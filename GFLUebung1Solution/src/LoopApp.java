
public class LoopApp
{
	public static void main(String[] args)
	{
		double result = 0;
		for (int index = 2; index <= 100; index++)
		{
			result += 1.0 / index;
		}
		System.out.println(result);
		
		int intResult =0;
		for(int index = 3 ; index < 9000;index = index+3)
		{
			intResult+=index;
		}
		
		System.out.println(intResult);
	}
}
