
public class Auto
{
	private String farbe;
	private double tankstand;
	private double geschwindigkeit;

	public Auto(String autofarbe) // konstruktor
	{
		farbe = autofarbe;
	}

	public void volltanken()
	{
		tankstand = 100;
	}

	public void beschleunigen(double gas)
	{
		geschwindigkeit = geschwindigkeit + gas;
		if (geschwindigkeit > 350)
		{
			geschwindigkeit = 350;
		}
		checkStatus();
	}

	public void langsamer(double bremsen)
	{
		geschwindigkeit = geschwindigkeit - bremsen;
		if (geschwindigkeit <= 0)
		{
			geschwindigkeit = 0;
		}
		checkStatus();
	}

	public void fahren(double zeit) // sekunden
	{
		tankstand = tankstand - (geschwindigkeit * zeit) / 70000;
		checkStatus();
	}

	private void checkStatus()
	{
		if (tankstand <= 0)
		{
			tankstand = 0;
			geschwindigkeit = 0;
		}
	}

	public void status()
	{
		System.out.printf("%s %.2f %.2f\n", farbe, geschwindigkeit, tankstand);
	}

	public String toString()
	{
		return farbe + " " + geschwindigkeit;
	}
}
