package edu.campus02.fruechte.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import edu.campus02.fruechte.Frucht;
import edu.campus02.fruechte.ObstKorb;

public class ObstkorbTest {
	@Test
	public void ablegen01() throws Exception {

		ObstKorb korb01 = new ObstKorb();

		korb01.fruchtAblegen(new Frucht("Apfel", 200));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		assertEquals("[(Apfel 200,00), (Birne 220,00), (Melone 1500,00)]", korb01.toString());
	}

	@Test
	public void schwertse01() throws Exception {

		ObstKorb korb01 = new ObstKorb();

		korb01.fruchtAblegen(new Frucht("Apfel", 200));
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));

		Frucht schwersteFrucht = korb01.schwersteFrucht();

		assertEquals(230, schwersteFrucht.getGewicht(), 0.1);
	}

	@Test
	public void schwertse02() throws Exception {

		ObstKorb korb01 = new ObstKorb();

		korb01.fruchtAblegen(new Frucht("Melone", 1500));
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));

		Frucht schwersteFrucht = korb01.schwersteFrucht();

		assertEquals(1500, schwersteFrucht.getGewicht(), 0.1);
	}

	@Test
	public void schwertse03() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		Frucht schwersteFrucht = korb01.schwersteFrucht();

		assertEquals(1500, schwersteFrucht.getGewicht(), 0.1);
	}

	@Test
	public void schwertse04() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));
		korb01.fruchtAblegen(new Frucht("Birne", 220));

		Frucht schwersteFrucht = korb01.schwersteFrucht();
		assertEquals(1500, schwersteFrucht.getGewicht(), 0.1);

		schwersteFrucht = korb01.schwersteFrucht();
		assertEquals(1500, schwersteFrucht.getGewicht(), 0.1);

	}

	@Test
	public void schwererAls01() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		ArrayList<Frucht> fruechteSchwererAls = korb01.fruechteSchwererAls(215);
		assertEquals(3, fruechteSchwererAls.size());
	}

	@Test
	public void schwererAls02() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		ArrayList<Frucht> fruechteSchwererAls = korb01.fruechteSchwererAls(2000);
		assertEquals(0, fruechteSchwererAls.size());
	}

	@Test
	public void schwererAls03() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Banane", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		ArrayList<Frucht> fruechteSchwererAls = korb01.fruechteSchwererAls(0);
		assertEquals(4, fruechteSchwererAls.size());
	}

	@Test
	public void zaehle01() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Birne", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		assertEquals(2, korb01.zaehleFruechte("Birne"));
	}

	@Test
	public void zaehle02() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Birne", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		assertEquals(0, korb01.zaehleFruechte("Mango"));
	}

	@Test
	public void zaehleSorte01() throws Exception {

		ObstKorb korb01 = new ObstKorb();
		korb01.fruchtAblegen(new Frucht("Birne", 230));
		korb01.fruchtAblegen(new Frucht("Apfel", 210));
		korb01.fruchtAblegen(new Frucht("Birne", 220));
		korb01.fruchtAblegen(new Frucht("Melone", 1500));

		assertEquals(3, korb01.zaehleSorte());
	}

}
