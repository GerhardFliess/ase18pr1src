package org.campus.filesystem;

import java.util.ArrayList;

public class Verzeichnis
{
	private ArrayList<Datei> dateien = new ArrayList<Datei>();
	private ArrayList<Verzeichnis> verzeichnisse = new ArrayList<Verzeichnis>();

	private String name;

	public Verzeichnis(String name)
	{
		this.name = name;
	}

	public void add(Datei neueDatei)
	{
		dateien.add(neueDatei);
	}

	public void add(Verzeichnis verzeichnis)
	{
		verzeichnisse.add(verzeichnis);
	}

	public String toString()
	{
		String verzeichnisse = "";
		for (Verzeichnis verzeichnis : this.verzeichnisse)
		{
			verzeichnisse += verzeichnis.toString() + " ";
		}

		return String.format("%s (%s) [%s]", name, dateien, verzeichnisse);
	}

	public int size()
	{
		int result = 0;
		for (Datei datei : dateien)
		{
			result += datei.getGroesse();
		}
		
		for (Verzeichnis verzeichnis : verzeichnisse)
		{
			result += verzeichnis.size();
		}
		return result;
	}
}
