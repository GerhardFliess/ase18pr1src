package org.campus.filesystem;

import javax.print.attribute.standard.Sides;

public class Datei
{
	private String name;
	private int groesse;

	public Datei(String name, int groesse)
	{

		this.name = name;
		this.groesse = groesse;
	}

	public String getName()
	{
		return name;
	}

	public int getGroesse()
	{
		return groesse;
	}

	public String toString()
	{
		return String.format("(%s,%d)", name, groesse);
	}
}
