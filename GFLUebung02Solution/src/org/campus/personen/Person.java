package org.campus.personen;

import java.util.ArrayList;

public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;

	private ArrayList<Person> kinder = new ArrayList<Person>();
	private static int anzahlPersonen;

	public Person(String vorname, String nachname)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		anzahlPersonen++;
	}

	public void setVater(Person vater)
	{
		this.vater = vater;
		vater.addKind(this);
	}

	public void setMutter(Person mutter)
	{
		this.mutter = mutter;
		mutter.addKind(this);
	}

	public Person getVater()
	{
		return vater;
	}

	public void addKind(Person kind)
	{
		kinder.add(kind);
	}

	public String toString()
	{
		String kinderString ="[ ";
		for (Person person : kinder)
		{
			kinderString+= person.vorname + " " +person.nachname+ " ";
		}
		kinderString+="]";
		
		if (mutter == null && vater == null)
		{
			return String.format("(%s %s (? ?)) Kinder:%s %d", vorname, nachname, kinderString, anzahlPersonen);
		}

		if (mutter == null)
		{
			return String.format("(%s %s (? %s)) Kinder:%s %d ", vorname, nachname, vater, kinderString, anzahlPersonen);
		}

		if (vater == null)
		{
			return String.format("(%s %s (%s ?)) Kinder:%s %d", vorname, nachname, mutter, kinderString, anzahlPersonen);
		}

		return String.format("(%s %s (%s %s)) Kinder:%s %d", vorname, nachname, mutter, vater, kinderString, anzahlPersonen);
	}

}
