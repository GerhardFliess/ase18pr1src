import org.campus.filesystem.Datei;
import org.campus.filesystem.Verzeichnis;

public class VerzeichnissApp
{
	public static void main(String[] args)
	{
		Datei d1 = new Datei("test1.txt", 100);
		Datei d2 = new Datei("test2.txt", 100);
		Datei d3 = new Datei("test3.txt", 100);
		Datei d4 = new Datei("test4.txt", 100);
		
		System.out.println(d1);
	
		
		Verzeichnis root = new Verzeichnis("c:/");
		root.add(d1);
		root.add(d2);
		root.add(d3);
		
		System.out.println(root);
		
		Verzeichnis v1 = new Verzeichnis("Dateien");
		v1.add(d4);
		root.add(v1);
		
		System.out.println(root);
		
		System.out.println(root.size());
	}
}
