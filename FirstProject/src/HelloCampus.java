
public class HelloCampus
{
	public static void main(String[] parameter)
	{
		System.out.println("Hello Campus!");

		int index; // Dekleration --> ich brauche eine Variable index
		index = 1; // initialisierung -> ich setze sie auf den wert 1
		index = 2;

		int zaehler = 2; // Deklaration und initialsierung

		index = index + 1; // z�hle 1 dazu
		index += 1; // Z�hle 1 dazu
		index++; // 1 dazu z�hlen

		double preis = 3.2;
		System.out.println(index);
	}

}
