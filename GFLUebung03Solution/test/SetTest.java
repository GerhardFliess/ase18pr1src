

import static org.junit.Assert.*;

import org.campus02.set.Set;
import org.junit.Test;

public class SetTest {
	@Test
	public void test01() throws Exception {

		Set set = new Set();
		set.add(3);
		set.add(4);
		set.add(1);

		assertEquals("[3, 4, 1]", set.toString());
	}
	
	@Test
	public void test02() throws Exception {

		Set set = new Set();
		set.add(3);
		set.add(4);
		set.add(1);

		assertEquals(8, set.sum());
	}

	@Test
	public void test03() throws Exception {

		Set set = new Set();
		set.add(3);
		set.add(4);
		set.add(1);

		Set set2 = new Set();
		set2.add(2);
		set2.add(3);

		Set set3 = new Set();
		set3.add(1);
		set3.add(4);
		
		Set set4 = new Set();
		set4.add(7);
		set4.add(8);

		set2.add(set3);
		set2.add(set4);
		
		set.add(set2);
		
		assertEquals("[3, 4, 1] [[2, 3] [[1, 4], [7, 8]]]", set.toString());
		assertEquals(33, set.sum());
	}
}
