package org.campus02.domino;

public class Tile {

	private int higher;
	private int smaller;

	public Tile(int h, int s) {
		if (h > s) {
			higher = h;
			smaller = s;
		} else {
			higher = s;
			smaller = h;
		}
	}

	public int getNumber1() {
		return higher;
	}

	public int getNumber2() {
		return smaller;
	}

	public boolean compare(Tile otherTile) {
		if (this.getNumber1() > otherTile.getNumber1()) {
			return false;
		}

		if (this.getNumber1() < otherTile.getNumber1()) {
			return true;
		}

		if (this.getNumber2() < otherTile.getNumber2()) {
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return String.format("(%d,%d)", getNumber1(), getNumber2());
	}
}
