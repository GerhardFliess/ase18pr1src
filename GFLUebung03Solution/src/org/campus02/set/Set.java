package org.campus02.set;

import java.util.ArrayList;

public class Set
{

	private ArrayList<Integer> items = new ArrayList<Integer>();
	private ArrayList<Set> sets = new ArrayList<Set>();

	public void add(int value)
	{
		items.add(value);
	}

	public void add(Set set)
	{
		sets.add(set);
	}

	public int sum()
	{
		int sumResult = 0;
		for (Integer integer : items)
		{
			sumResult += integer;
		}

		for (Set set : sets)
		{
			sumResult += set.sum();
		}
		return sumResult;
	}

	@Override
	public String toString()
	{
		if (sets.isEmpty())
		{
			return items.toString();
		} else
		{
			return String.format("%s %s", items.toString(), sets.toString());
		}
	}
}
