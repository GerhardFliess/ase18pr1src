package at.campus02.ase.messages;

import java.util.ArrayList;

public class Message
{
	private String title;
	private String text;
	private ArrayList<Message> antworten = new ArrayList<Message>();

	public Message(String titel, String text)
	{
		this.title = titel;
		this.text = text;
	}

	public Message antworten(String titel, String text)
	{

		Message temp = new Message(titel, text);
		antworten.add(temp);
		return temp;
	}

	public ArrayList<Message> getAntworten()
	{
		return antworten;
	}

	public int countMessages()
	{

		int result = antworten.size();
		
		for (Message message : antworten)
		{
			result += message.countMessages();
		}
		return result;

	}

	public String toString()
	{
		if (antworten.isEmpty())
		{
			return String.format("(%s,%s)", title, text);
		} else
		{
			return String.format("(%s,%s)%s", title, text, antworten);

		}
	}
}
