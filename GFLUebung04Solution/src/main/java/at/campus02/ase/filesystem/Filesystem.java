package at.campus02.ase.filesystem;

import java.util.ArrayList;

public class Filesystem
{
	private ArrayList<File> dateien = new ArrayList<File>();

	public void addFile(File b)
	{
		dateien.add(b);
	}

	public int countSize()
	{
		int result = 0;

		for (File file : dateien)
		{
			result += file.getSize();
		}
		return result;
	}

	public double averageSize()
	{
		double countSize = this.countSize();

		return countSize / dateien.size();
	}

	public ArrayList<File> fileByOwner(String owner)
	{
		ArrayList<File> result = new ArrayList<File>();

		for (File file : dateien)
		{
			if (file.getOwner() == owner)
			{
				result.add(file);
			}
		}
		return result;
	}

	public ArrayList<File> findFile(String search)
	{
		ArrayList<File> result = new ArrayList<File>();

		if (search == null)
			return result;
		
		for (File file : dateien)
		{
			if (file.getOwner().contains(search) || file.getName().contains(search))
			{
				result.add(file);
			}
		}
		return result;
	}
}
