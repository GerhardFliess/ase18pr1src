import at.campus02.ase.filesystem.File;
import at.campus02.ase.filesystem.Filesystem;

public class FileSystemApp {

	public static void main(String[] args) {
		
		File f1 = new File("Hansi", "Datei1", 46);
		File f2 = new File("Ana", "Datei2", 125);
		File f3 = new File("Adri", "Datei3", 160);

		Filesystem fs = new Filesystem();
		
		fs.addFile(f1);
		fs.addFile(f2);
		fs.addFile(f3);
		

		System.out.println(fs.countSize());
	}



}


