
public class StringApp
{

	public static void main(String[] args)
	{
		String text = "abdc"; // --> new String("abcd");

		System.out.println(text.toUpperCase());
		System.out.println(text.length());
		System.out.println(text.charAt(2)); // zeichen an der position 2 (start bei 0)

		String werte = "1,b,hansi,mimi,ball,444";
		System.out.println();
		System.out.println(werte.indexOf("mimi"));
		System.out.println(werte.indexOf("MIMI"));

		
		System.out.println();
		String[] split = werte.split(",");
		
		
		for (String string : split)
		{
			System.out.println(string);
		}
		
		String mitLeerzeichen = "  iguzg  ";
		
		System.out.println(mitLeerzeichen);
		System.out.println(mitLeerzeichen.trim());
		

	}

}
